package Pages;

import QAbase.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.TakesScreenshot;

public class LoginPage extends TestBase {


    //PageFactory-OR-Object Reporisotories

    @FindBy(name = "email")
    WebElement email;

    @FindBy(name = "password")
    WebElement password;

    @FindBy(xpath = " //span[@class='mdl-button__ripple-container']")
    WebElement loginBtn;

    @FindBy(xpath = "//a[@class='forgotPassword']")
    WebElement passwortVergessenBtn;

    @FindBy(xpath = "//div[@class='loginlogocontainer']")
    WebElement MRLogo;

    ////////////////////test13:555

    //Initializing pageojects
    public LoginPage() {
        PageFactory.initElements(driver, this);
    }

    //Actions
    public String validateLoginPageTitle() {
        return driver.getTitle();
    }

    public boolean validateMRImage() {
        return MRLogo.isDisplayed();
    }

    public Maklersuche login(String un, String pwd) {

        email.sendKeys(un);
        password.sendKeys(pwd);
        loginBtn.click();
        responseTime();
        return new Maklersuche();


    }
}
