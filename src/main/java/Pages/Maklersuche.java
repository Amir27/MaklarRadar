package Pages;

import QAbase.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class Maklersuche extends TestBase {


    //Menu//HOMEPAGE-Maklersuche
    @FindBy(xpath = "//div[@class='logoname']")                                                    WebElement menuLogoname;
    @FindBy(xpath = "//div[@class='menuBetreuer']")                                                WebElement menuBetreuer;
    @FindBy(xpath = "//button[@id='maklersuche']")                                                 WebElement menuMaklersuche;
    @FindBy(xpath = "//button[@id='profil']")                                                      WebElement menuProfil;
    @FindBy(xpath = "//button[@id='/html[1]/body[1]/header[1]/div[1]/div[4]/div[1]']")             WebElement menuProfilPassiv;
    @FindBy(xpath = "//button[@id='stammdaten']")                                                  WebElement menuStammdaten;
    @FindBy(xpath = "//button[@id='//div[2]//div[2]']")                                            WebElement menuStammdatenPassiv;
    @FindBy(xpath = "//button[@id='geschaeftsverlauf']")                                           WebElement menuGeschaeftsverlauf;
    @FindBy(xpath = "//button[@id='//div[@class='untermenu']//div[3]']")                           WebElement menuGeschaeftsverlaufPassiv;
    @FindBy(xpath = "//button[@id='zukunftsorientierung']")                                        WebElement menuZukunftsorientierung;
    @FindBy(xpath = "//button[@id='//div[4]//div[4]']")                                            WebElement menuZukunftsorientierungPassiv;
    @FindBy(xpath = "//button[@id='markt']")                                                       WebElement menuMarkt;
    @FindBy(xpath = "//button[@id='//div[@class='untermenu']//div[5]']")                           WebElement menuMarktPassiv;
    @FindBy(xpath = "//button[@id='vergleich']")                                                   WebElement menuVergleich;
    @FindBy(xpath = "//button[@id='//div[@class='untermenu']//div[6]']")                           WebElement menuVergleichPassiv;
    @FindBy(xpath = "//button[@id='suchhistorie']")                                                WebElement menuSuchhistorie;
    @FindBy(xpath = "//button[@id='portfolio']")                                                   WebElement menuPortfolio;
    @FindBy(xpath = "//button[@id='selektion']")                                                   WebElement menuSelektion;

    //MAKLERSUCHE-funktion
    @FindBy(xpath = "//h3[contains(text(),'EINGABE MAKLERSUCHE')]")                                WebElement titleEingabeMaklersuche;
    @FindBy(xpath = "//input[@id='sample1']")                                                      WebElement feld_FirmaGeschaeftsfuehrerAnsprechpartner;
    @FindBy(xpath = "//input[@id='sample2']")                                                      WebElement feld_PLZOrtStrasse;

    //MAKLERSUCHE-Funktion-AfterSuche

    public Maklersuche() {

        PageFactory.initElements(driver, this);
    }


    public boolean validateMRlogoLinks() {
        return menuLogoname.isDisplayed();
    }
    public boolean validateMRbetruer() {
        return menuBetreuer.isDisplayed();
    }
    public boolean validateTitleEingabeMaklersuche(){
        return titleEingabeMaklersuche.isDisplayed();

    }

    public Maklersuche menuMaklersucheClick(){
        menuMaklersuche.click();
        responseTime();
        return new Maklersuche();
    }

    public Profil menuProfilClick() {
        menuProfil.click();
        return new Profil();
    }
     public Profil menuProfilPassivClick() {
           menuProfil.click();
       ///what happ
return null;
       }
       public Stammdaten menuStammdatenClick() {
           menuStammdaten.click();
           return new Stammdaten();
       }
        public Stammdaten menuStammdatenPassivClick() {
              menuStammdaten.click();
            return null;
          }
          public Geschaeftsverlauf menuGeschaeftsverlaufClick() {
              menuGeschaeftsverlauf.click();
              return new Geschaeftsverlauf();
          }
            public Geschaeftsverlauf menuGeschaeftsverlaufPassivClick() {
                menuGeschaeftsverlauf.click();
                return null;
            }

          public Zukunftsorientierung menuZukunftsorientierungClick() {
                menuZukunftsorientierung.click();
                return new Zukunftsorientierung();
            }
              public Zukunftsorientierung menuZukunftsorientierungPassivClick() {
                  menuZukunftsorientierung.click();
                  return null;
              }

    public Markt menuMarktClick() {
        menuMarkt.click();
        return new Markt();
    }
              public Markt menuMarktPassivClick() {
                  menuMarkt.click();
                  return null;
              }

    public Vergleich menuVergleichClick() {
        menuVergleich.click();
        return new Vergleich();
    }
   public Vergleich menuVergleichPassivClick() {
        menuVergleich.click();
       return null;
    }

    public Suchhistorie menuSuchhistorieClick() {
        menuSuchhistorie.click();
        responseTime();

        return new Suchhistorie();
    }

    public Portfolio menuPortfolioClick() {
        menuPortfolio.click();
        responseTime();
        return new Portfolio();
    }
    public Selektion menuSelektionClick() {
        menuSelektion.click();
        responseTime();
        return new Selektion();
    }



}
