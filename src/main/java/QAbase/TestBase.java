package QAbase;

import Utilities.TestUtil;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import io.github.bonigarcia.wdm.DriverManagerType;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {



     public static WebDriver driver;
     public static Properties prop;


    public TestBase(){


        try {

            prop =new Properties();
            FileInputStream ip=new FileInputStream("C:\\Users\\k.topcak\\Desktop\\MR\\src\\main\\java\\Config\\config.properties");
            prop.load(ip);



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }

    }


public static void initialization(){

        String browserName=prop.getProperty("browser");


            if (browserName.contains("Chrome")) {
                WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
                driver = new ChromeDriver();
            }
            else if (browserName.contains("Firefox")){
                WebDriverManager.getInstance(DriverManagerType.FIREFOX).setup();
                driver= new FirefoxDriver();
            }


            driver.manage().window().maximize();
            driver.manage().deleteAllCookies();
            driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
            driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT,TimeUnit.SECONDS);


            driver.get(prop.getProperty("url"));
        }

        public static void responseTime(){

            String Alo = driver.getCurrentUrl();
            Response response= RestAssured.get(Alo);
            long timemilli = response.time();
            System.out.println("Response time of " +Alo+ " is " +timemilli);
//cacheyi temizle:)

    }



}






