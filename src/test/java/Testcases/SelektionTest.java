package Testcases;

import Pages.LoginPage;
import Pages.Maklersuche;
import Pages.Selektion;
import Pages.Suchhistorie;
import QAbase.TestBase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SelektionTest extends TestBase {



    LoginPage loginPage;
    Maklersuche homepage;
    Selektion selektion;

    public SelektionTest(){

        super();//base class properties will be called
    }


    @BeforeMethod
    public void setUp() {

        initialization();//dirver info will be initilized
        loginPage=new LoginPage();
        homepage = loginPage.login(prop.getProperty("email"),prop.getProperty("password"));//Erfolgreiche Login nach config
        selektion = homepage.menuSelektionClick();
    }


    @Test
    public void titleSelektionsfilterTest(){

        boolean flag= selektion.titleSelektionsfilter();
        Assert.assertTrue(flag);



    }




}
