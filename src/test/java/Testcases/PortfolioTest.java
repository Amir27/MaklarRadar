package Testcases;

import Pages.LoginPage;
import Pages.Maklersuche;
import Pages.Portfolio;
import QAbase.TestBase;
import org.junit.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PortfolioTest extends TestBase {

        LoginPage loginPage;
        Maklersuche homepage;
        Portfolio portfolio;

        public PortfolioTest(){

            super();//base class properties will be called
        }


        @BeforeMethod
        public void setUp() {

            initialization();//dirver info will be initilized
            loginPage=new LoginPage();
            homepage = loginPage.login(prop.getProperty("email"),prop.getProperty("password"));//Erfolgreiche Login nach config
            portfolio = homepage.menuPortfolioClick();

        }


        @Test
        public void titleSuchhistorieTest(){

            boolean flag= portfolio.titleAnzeigeoptionen();
            Assert.assertTrue(flag);



        }




    }
