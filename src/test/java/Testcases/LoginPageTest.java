package Testcases;

import Pages.LoginPage;
import Pages.Maklersuche;
import QAbase.TestBase;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LoginPageTest extends TestBase {


    LoginPage loginPage;
    Maklersuche maklersuche;



    public LoginPageTest(){

      super();


    }

@BeforeMethod
    public void setUp() {

    initialization();
    loginPage = new LoginPage();
}




/*
@Test(priority = 1)
public void loginPageTitleTest() {


        String title = loginPage.validateLoginPageTitle();

    System.out.println(""+title);
    Assert.assertEquals(title, "Maklerradar");
}
*/
@Test(priority = 1)
public void MRLogoImageTests() {

    boolean flag = loginPage.validateMRImage();
    Assert.assertTrue(flag);
}

@Test(priority = 2)
        public void loginTest(){

    maklersuche=loginPage.login(prop.getProperty("email"),prop.getProperty("password"));


        }

@AfterMethod
public void tearDown(){


  //  driver.quit();
    }


}





