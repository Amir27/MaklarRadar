package Testcases;

import Pages.LoginPage;
import Pages.Maklersuche;
import QAbase.TestBase;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MaklersucheTest extends TestBase {
    LoginPage loginPage;
    Maklersuche homepage;

public MaklersucheTest(){

    super();//base class properties will be called
}


    @BeforeMethod
    public void setUp() {

        initialization();//dirver info will be initilized
        loginPage=new LoginPage();
       homepage = loginPage.login(prop.getProperty("email"),prop.getProperty("password"));//Erfolgreiche Login nach config
    }


    @Test(priority = 1)
    public void validateMRlogoLinksTest() {

   boolean flag = homepage.validateMRlogoLinks();
       Assert.assertTrue(flag);

}
    @Test
   public void validateMRbetruerTest(){
    boolean flag=homepage.validateMRbetruer();
    Assert.assertTrue(flag);

    }

    @Test
    public void validateTitleEingabeMaklersucheTest(){

        boolean flag = homepage.validateTitleEingabeMaklersuche();
        Assert.assertTrue(flag);
}

    @Test
    public void menuMaklersucheClickTest(){
homepage.menuMaklersucheClick();

    }
    @Test
    public void menuSuchhistorieClickTest(){
homepage.menuSuchhistorieClick();

    }
    @Test
    public void menuPortfolioClickTest(){
homepage.menuPortfolioClick();

    }
    @Test
    public void menuSelektionClickTest(){
homepage.menuSelektionClick();

    }

    @AfterMethod
    public void tearDown(){


   // driver.quit();
    }



}







