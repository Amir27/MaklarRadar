package Testcases;

import Pages.LoginPage;
import Pages.Maklersuche;
import Pages.Suchhistorie;
import QAbase.TestBase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SuchhistorieTest extends TestBase {

    LoginPage loginPage;
    Maklersuche homepage;
    Suchhistorie suchhistorie;

    public SuchhistorieTest(){

        super();//base class properties will be called
    }


    @BeforeMethod
    public void setUp() {

        initialization();//dirver info will be initilized
        loginPage=new LoginPage();
        homepage = loginPage.login(prop.getProperty("email"),prop.getProperty("password"));//Erfolgreiche Login nach config
        suchhistorie = homepage.menuSuchhistorieClick();

    }


    @Test
    public void titleSuchhistorieTest(){

        boolean flag= suchhistorie.titleSuchhistorie();
        Assert.assertTrue(flag);



    }




}
